import { useEffect, useState } from 'react';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import avatar from '../avatar1.png';
import useStyles from '../useStyles';
import { connect } from 'react-redux';

function ListCardPage(props){
  const classes = useStyles();

  return(
    <div className={classes.contentContainer}>
      <div className={classes.cardsContainer}>
        {props.student.map((item, index) => (
          <Card key={index} className={classes.card} >
            <img src={avatar} className={classes.cardAvatar} />
            <div className={classes.cardTitle}>
              <Typography variant="subtitle2">{ item.name }</Typography>
            </div>
          </Card>
        ))}
      </div>
    </div>
  );
}

const mapStateToProps = state => {
  return {
    student: state.student
  };
}

export default connect(mapStateToProps)(ListCardPage);