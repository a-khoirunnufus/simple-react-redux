import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import AddIcon from '@material-ui/icons/Add';
import RecentActorsIcon from '@material-ui/icons/RecentActors';
import TableChartIcon from '@material-ui/icons/TableChart';
import { NavLink as NavLinkDefault } from "react-router-dom";
import useStyles from '../useStyles';
import { connect } from 'react-redux';

function Navbar(props){
  const classes = useStyles();
  const reset = {color: 'inherit', '& div': {backgroundColor: 'white'}};
  const active = {backgroundColor: 'white'};
  const title = () => {
      if(props.page === 'form') return "Add Data"
      else if(props.page === 'card') return "List Card"
      else if(props.page === 'table') return "Table"
      else return "UNKNOWN_PAGE";
  }
  const NavLink = ({children, to}) => (
    <NavLinkDefault to={to} style={{color: 'inherit'}}>
      {children}
    </NavLinkDefault>
  );

  return (
    <AppBar position="static">
      <Toolbar className={classes.toolbar} >
        <div style={{width: '100px', textAlign: 'left'}}>
          <Typography>{ title() }</Typography>
        </div>

        <NavLink to="/">
          <IconButton onClick={() => props.handleNav('form')} className={classes.toolbarItems} color="inherit">
            <AddIcon />
          </IconButton>
        </NavLink>

        <NavLink to="/card">
          <IconButton onClick={() => props.handleNav('card')} className={classes.toolbarItems} color="inherit">
            <RecentActorsIcon />
          </IconButton>
        </NavLink>

        <NavLink to="/table">
          <IconButton onClick={() => props.handleNav('table')} className={classes.toolbarItems} color="inherit">
            <TableChartIcon />
          </IconButton>
        </NavLink>
      </Toolbar>
    </AppBar>
  );
}

const mapStateToProps = state => {
  return {
    page: state.page
  };
}
const mapDispatchToProps = dispatch => {
  return {
    handleNav: (value) => dispatch({type: 'NAVIGATE_PAGE', value: value})
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Navbar);
