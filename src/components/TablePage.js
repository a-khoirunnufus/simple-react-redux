import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';

import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import useStyles from '../useStyles';
import { connect } from 'react-redux';

function TablePage(props){
  const classes = useStyles();

  const handleDel = (e) => {
  	props.delData(e);
  	console.log(e);
  	alert("data successfully deleted!");
  }

  return(
    <div className={classes.contentContainer}>
      
      <Card className={classes.editForm__container, classes.disableComp} elevation={4}>
        <CardHeader 
          className={classes.editForm__header}
          title={<Typography variant="h5" color="inherit">Edit Student</Typography>}
        />
        <CardContent className={classes.editForm__body}>
          <TextField name="name" id="name" label="Name" size="small" />
          <TextField name="sClass" id="sClass" label="Student Class" size="small" />
          <Button variant="contained" color="primary">Edit</Button>
        </CardContent>
      </Card>

      <div className={classes.tableContainer}>
		<TableContainer component={Paper}>
		  <Table>
		    <TableHead>
		      <TableRow>
		        <TableCell>Name</TableCell>
		        <TableCell>SID</TableCell>
		        <TableCell>Class</TableCell>
		        <TableCell align="center">ACTION</TableCell>
		      </TableRow>
		    </TableHead>
		    <TableBody>
		      {props.student.map((item, index) => (
		      	<TableRow key={index}>
			      <TableCell>{item.name}</TableCell>
			      <TableCell>{item.sid}</TableCell>
			      <TableCell>{item.sClass}</TableCell>

			      <TableCell align="center">
			        <IconButton style={{color: 'green'}}>
			          <EditIcon />
			        </IconButton>
			        <IconButton onClick={ () => handleDel(item.sid) } style={{color: 'red'}}>
			          <DeleteIcon />
			        </IconButton>
			      </TableCell>
			    </TableRow>
		      ))}
		    </TableBody>
		  </Table>
		</TableContainer>
	  </div>
    </div>
  );
}

const mapStateToProps = state => {
  return {
  	student: state.student
  };
}

const mapDispatchToProps = dispatch => {
  return {
  	delData: (value) => dispatch({type: 'DEL_DATA', value: value})
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TablePage);