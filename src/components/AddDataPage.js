import { useState } from 'react';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import useStyles from '../useStyles';
import { connect } from 'react-redux';

function AddDataPage(props){
  const classes = useStyles();
  const defaultState = {
    name: '',
    sid: '',
    sClass: ''
  };
  const [state, setState] = useState(defaultState);

  const handleChange = (e) => {
    setState({
      ...state,
      [e.target.name]: e.target.value
    })
  }

  const handleSubmit = () => {
    props.addData(state);
    setState(defaultState);
    alert("data successfully added!");
  }

  return(
    <div className={classes.contentContainer}>
      <Paper className={classes.formContainer}>
        <form className={classes.form} autoComplete="off">
          <TextField onChange={ e => handleChange(e) } value={state.name} name="name" id="name" label="Name" size="small" />
          <TextField onChange={ e => handleChange(e) } value={state.sid} name="sid" id="sid" label="SID" size="small" />
          <TextField onChange={ e => handleChange(e) } value={state.sClass} name="sClass" id="sClass" label="Student Class" size="small" />
          <Button onClick={ () => handleSubmit() } variant="contained" color="primary" style={{marginTop: '1rem', width: '100%'}}>Add</Button>
        </form>
      </Paper>
    </div>
  );
};

const mapDispatchToProps = dispatch => {
  return {
    addData: (value) => dispatch({
      type: 'ADD_DATA', 
      value: value,
  })}
};

export default connect(null, mapDispatchToProps)(AddDataPage);