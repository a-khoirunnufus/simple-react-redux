const stateEx =  [
  {
  	name: 'Ahmad Khoirunnufus',
  	sid: '1486672257',
  	sClass: 'AB0303'
  },
  {
  	name: 'Budi Budiman',
  	sid: '8754403316',
  	sClass: 'AL8723'
  },
  {
  	name: 'Cici Prayoga',
  	sid: '1347783325',
  	sClass: 'FT4638'
  },
  {
  	name: 'Dodo Nurbaiti',
  	sid: '1459982265',
  	sClass: 'FY6769'
  },
  {
  	name: 'Endah Budigirl',
  	sid: '1679942246',
  	sClass: 'YU1658'
  }
]

export default stateEx;