import { render } from 'react-dom';
import React, { useState } from 'react';
import './style.css';
import { useSpring, animated, config } from 'react-spring';
import { Provider } from 'react-redux';
import { ConfigureStore } from './redux/configureStore';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  withRouter
} from "react-router-dom";

// custom component
import Navbar from './components/Navbar';
import AddDataPage from './components/AddDataPage';
import ListCardPage from './components/ListCardPage';
import TablePage from './components/TablePage';
import useStyles from './useStyles';

const store = ConfigureStore();

function App() {
  return (
    <div>
      <Provider store={store}>
        <Router>
          <Navbar />
          <Switch>
            <Route exact path="/">
              <AddDataPage />
            </Route>
            <Route path="/card">
              <ListCardPage />
            </Route>
            <Route path="/table">
              <TablePage />
            </Route>
          </Switch>
        </Router>
      </Provider>
    </div>
  );
}

render(<App />, document.getElementById('root'));