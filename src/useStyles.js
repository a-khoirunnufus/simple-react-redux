import { makeStyles } from '@material-ui/core/styles';
import { deepOrange, deepPurple } from '@material-ui/core/colors';

const useStyles = makeStyles((theme) => ({
  toolbar: {
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  toolbarItems: {
  	marginLeft: '.5rem',
  	marginRight: '.5rem',
  },
  contentContainer: {
  	backgroundColor: '#fafafa',
  	width: '100%',
  	height: 'calc(100vh - 64px)',
  	display: 'relative',
  	paddingTop: '3rem'
  },
  formContainer: {
  	display: 'flex',
  	flexDirection: 'column',
  	alignItems: 'center',
  	width: '300px',
  	padding: '2rem',
  	position: 'absolute',
  	left: '50%',
  	marginLeft: '-150px',
  },
  form: {
  	display: 'flex',
  	flexDirection: 'column',
    '& div': {
      marginBottom: '.5rem'
    }
  },
  cardsContainer: {
  	width: '80%',
  	marginLeft: 'auto',
  	marginRight: 'auto',
  	display: 'flex',
  	justifyContent: 'center',
    flexWrap: 'wrap'
  },
  card: {
  	width: '120px',
  	textAlign: 'center',
  	padding: '.7rem',
  	margin: '1rem',
  },
  cardAvatar: {
  	width: '90px', 
    height: '90px', 
    borderRadius: '45px',
    margin: '0 auto .5rem auto',
  },
  cardTitle: {
  	marginTop: '.5rem',
  },
  tableContainer: {
    width: '600px',
    marginRight: 'auto',
    marginLeft: 'auto'
  },
  editForm__container: {
    width: '300px',
    position: 'absolute'
  },
  editForm__header: {
    backgroundColor: theme.palette.primary.main,
    color: '#fff',
    padding: '1rem'
  },
  editForm__body: {
    width: '300px',
    display: 'flex',
    flexDirection: 'column',
    padding: '2rem',
    '& div': {
      marginBottom: '1rem'
    }
  },
  disableComp: {
    display: 'none'
  }
}));

export default useStyles;