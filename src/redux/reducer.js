import { useEffect } from 'react';
import stateEx from '../state-ex';

export const initialState = {
	student: stateEx,
	page: 'card'
}

export const Reducer = (state = initialState, action) => {
  console.log("reducer running");
  if(action.type === 'NAVIGATE_PAGE'){
  	return {
  	  ...state,
  	  page: action.value
  	}
  }
  if(action.type === 'ADD_DATA'){
  	let temp = state.student;
  	temp.push(action.value);
  	return {
  	  ...state,
  	  student: temp
  	}
  }
  if(action.type === 'DEL_DATA'){
  	let temp = state.student.filter(idv => idv.sid!=action.value);
  	return {
  	  ...state,
  	  student: temp
  	}
  }
  return state;
}